# Read Me

The following steps will tell you how to create a local GitLab instance and install a local gitlab runner.
This is currently only implemented for windows builds running on a windows 10 machine.

## Requirements

- Docker for Windows
- Unity Hub (latest)
- Unity 2019.2.12f1 

## Start GitLab

Run
```
docker-compose -f docker/docker-compose.yml up -d
```

Open GitLab: [http://localhost]()

The initial root credentials are:
```
user: root
password: 5iveL
```
The password needs to be changed after the first login.


## Intstall the Gitlab Runner for Windows

- create a seperate user. Open ```compmgmt.msc``` and create a user called gitlab-runner and add him to the admin group
- login once to create the home direcory this will allow you to change the runners global ```.gitconfig```.
- to register a shared runner, open GitLab and go to the Admin Area, then select Runners. There you will find the token
- open an elevated Powershell and navigate to the ```./runner``` directory.
- execute:
```powershell
.\gitlab-runner.exe install --user ".\gitlab-runner" --password ENTER-YOUR-PASSWORD
.\gitlab-runner.exe start
.\gitlab-runner.exe register
```
- Follow the prompt to register the runner
- Set the executor to: ```shell```
- Add tags ```windows, unity```
- Add the environment variables and change the ```concurrent``` value in the ```config.toml```

```toml
concurrent = 2
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "my-demo-runner"
  url = "http://localhost/"
  token = "THE_TOKEN"
  executor = "shell"
  shell = "powershell"
  [environment]  
    UNITY_HUB_PATH = "C:/Program Files/Unity Hub/Unity Hub.exe"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

## Add the Demo Repo

Crate an empty repository in GitLab add it as new remote to this repository.
Push this repository into the created repository.

```
git remote add demo https://localhost/path/to/myrepo.git
git push origin demo
```
Make a tag and see if the build works.

Note: This demo requires Unity 2019.2.12f1. if you want to use a different version change it in the ```.gitlab-ci.yml``` file


## Test Build Localy

Open a and navigate to the repository root. Execute:
```powershell
npm i

$env:UNITY_CACHE_SERVER = "localhost:8126" 
$env:UNITY_HUB_PATH = "C:/Program Files/Unity Hub/Unity Hub.exe"
$env:UNITY_VERSION = "2019.2.12f1"
$env:CI_COMMIT_MESSAGE = "Hello World"
$env:CI_JOB_ID = "0"
$env:CI_COMMIT_TAG = "0.0.1"
$env:BUILD_TARGET = "StandaloneWindows64"
$env:BUILD_PREFIX = "demo"
$env:OUTPUT_DIR = "build"
$env:BACKEND_BASE_URL: "https://test.mydomain.com/mygame" 

npx gulp buildWindows
```

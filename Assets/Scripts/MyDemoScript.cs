﻿using UnityEngine;
using UnityEngine.UI;

public class MyDemoScript : MonoBehaviour {
    public Text infoText;
    
    void Awake() {
        var mySettings = Resources.Load<MySettings>("MySettings");
        infoText.text = mySettings.ToString();
    }

}
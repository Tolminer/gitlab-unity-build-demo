﻿using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName ="MySettings.asset",menuName ="Demo/CreateSettings")]
public class MySettings : ScriptableObject {
    public string jobId ="?";
    public string pipelineId = "?";
    public string commitRef = "?";
    public string commitHash = "?";
    public string commitMessage = "?";
    public string backendBaseURL = "?";

    public override string ToString() {
        var sb = new StringBuilder();
        sb.AppendFormat("jobId: {0}\n",jobId);
        sb.AppendFormat("pipelineId: {0}\n",pipelineId);
        sb.AppendFormat("commitRef: {0}\n",commitRef);
        sb.AppendFormat("commitMessage: {0}\n",commitMessage);
        sb.AppendFormat("commitHash: {0}\n",commitHash);
        sb.AppendFormat("backendBaseURL: {0}\n",backendBaseURL);
        return sb.ToString();
    }

}
